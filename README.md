# README #

welcome to standalone , an application written in PHP to fetch big data from twitter and store them in mongodb.

### What is this application for? ###

* this application does the following 2 tasks in parallel 
First: 
get the first top 10000 results of search from twitter provided by search word 

Second:
 get the first recent 10000 results of search from twitter provided by search word 

- Store the data in mongo database ( we need tweet and user_uid , datetime )
- Store each tweet user informations in different collection 
- Store the top 10 users in site parse.com
 ( users who has the highest followers ) in different collection 

* Version 0.1

### Server Requirments ###

* LAMP server , mysql db not necessary
* Phalcon framework installed
* Beanstalkd server installed
* mongodb database installed
* api keys from twitter developer accounts
* api keys from parse accounts

### how to install ###

* download the script files to web server with the above requirments
* edit app/config/config.php and enter all required data
* configure Beanstalkd to have 2 method in worker class (toptweet and recenttweet) ,
	toptweet will recieve searchword and send it as http post request to recentAction in index controller
	recenttweet will recieve searchword and send it as http post request to topAction in index controller

### have a question? ###
* Please contact me amr@amrmahrous.com