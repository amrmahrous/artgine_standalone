<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => '',
        'password'    => '',
        'dbname'      => '',
        'charset'     => 'utf8',
    ),
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => '/standalone/',
    ),
    'beanstalk' => array(
        'host' => '127.0.0.1',
        'port'      => '11300',
    ),
    'TwitterOAuth' => array(
        'path'     => APP_PATH . '/app/plugins/TwitterOAuth/',
        'consumer_key' => 'J4fiNjIx3qe3ffRTl2m3SfZ6R',
        'consumer_secret' => 'MfQACPGZ0lOBmH1E5TEwqAgE6aC9te7e8Ar9Ra2T1oMubg9T7I',
        'access_token' => '119774640-5mXDymPjecuGLSn81ByU1QhUNqoSQRLLQaN7blx1',
        'access_token_secret'      => '98LoeZUobHVOJtYo21BSOjuftAguqdqbemNS1WZuMNxGZ',
    ),
    'mongo' => array(
        'dbname'     => 'standalone',
    ),

    'parse' => array(
        'path'     => APP_PATH . '/app/plugins/Parse/',
        'app_id' => '96w6FUupZE5Foq5JcWSWktnYAZVdULH8yw4QXNtn',
        'rest_key' => 'mfEuRgNPT12n02luwE71tm2Kcw7x6fsP1vX3svFQ',
        'master_key' => 'ftr95n2944EH3F13J2swE5PP5HsxHoHy4spFepPG',
    )

));
