<?php

$loader = new \Phalcon\Loader();


$loader->registerNamespaces(
    array(
        'Abraham\TwitterOAuth'      => $config->TwitterOAuth->path,
        'Parse'      => $config->parse->path
    )
);

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
		$config->application->pluginsDir,
		$config->TwitterOAuth->path,
        $config->application->modelsDir
    )
)->register();


