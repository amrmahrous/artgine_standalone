<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
    	
    }
    /*
		search controller will get the seach word from site visitor 
		then send it to beanstalkd service
		beanstalkd service shall be configured to hit  recentAction and topAction using http post request
    */
    public function searchAction()
    {
    	$this->view->searchWord = NULL;
    	// Check if request has made with POST
        if ($this->request->isPost()) {

        	//	sanitize search word;
			$filter = new Phalcon\Filter();
			$searchWord  = $filter->sanitize($this->request->getPost("searchword"), "string");
            $this->view->searchWord = $searchWord;
           /* 
	    	retrieve Beanstalk server configuration
	    	edit configuration at config/config.php
    		*/
    	$bansconf = array();
		$bansconf['host'] = $this->config->beanstalk->host;
		$bansconf['port'] = $this->config->beanstalk->port;

		// Connect to the queue

		$queue = new Phalcon\Queue\Beanstalk($bansconf);
		/* 
		 Insert jobs in the queue , beanstalkd worker will have 2 methods (toptweet and recenttweet)
		 which shall be configured to call http://script-url/index/top and http://script-url/index/recent
		 */
		$toptweetId = $queue->put(array('toptweet' => $searchWord));
		$recenttweetId = $queue->put(array('recenttweet' => $searchWord));

        }
        // if no post request , redirect to form page
        else{
        	$response = new \Phalcon\Http\Response();
        	$response->redirect("index");
        	return $response;
        }

    }
    	/*
			A controller to be called by beanstalkd server with post request (searchword post) 
			to fetch and store recent tweets 
			then send top 10 users to parse.com
    	*/
    public function recentAction(){

		$filter = new Phalcon\Filter();
		$searchWord  = $filter->sanitize($request->getPost("searchword"), "string");
  		$this->_storetwitts('recent',$searchWord);
  		$this->_parseusers();
	 }

	 	/*
			A controller to be called by beanstalkd server with post request (searchword post) 
			it fetch and store polpular tweets 
			it send top 10 users to parse.com
    	*/
    public function topAction(){
    	
		$filter = new Phalcon\Filter();
		$searchWord  = $filter->sanitize($request->getPost("searchword"), "string");
     	$this->_storetwitts('popular',$searchWord);
     	$this->_parseusers();
    }

		/*
			A method to get top 10 users from collection and send them to parse.com
    	*/
    private function _parseusers(){
    	
		$Tusers = Tusers::find(array("sort"  => array("followers_count" => 1),"limit" => 10));
		/*
			to send top 10 users to parse , I used parse PHP SDK library 
			the library is stored in plugins folder
			See documentation at https://parse.com/docs
			configuration for this library in config/config.php
    	*/
		Parse\ParseClient::initialize($this->config->parse->app_id, $this->config->parse->rest_key, $this->config->parse->master_key);
    	foreach ($Tusers as $tuser) {
    		$users = Parse\ParseObject::create($tuser->id);
			$user->set("uid", $tuser->id);
			$user->set("name", $tuser->name);
			$user->set("screen_name", $tuser->screen_name);
			$user->set("location", $tuser->location);
			$user->set("description", $tuser->description);
			$user->set("url", $tuser->url);
			$user->set("followers_count", $tuser->followers_count);
			$user->set("friends_count", $tuser->friends_count);
			$user->set("created_at", $tuser->created_at);
			$user->set("lang", $tuser->lang);
			$user->set("following", $tuser->following);
			$user->save();
    	}
    }


		/*
			A method to get tweet from twitter api and store them in a collection
    	*/

    private function _storetwitts($type='recent',$word='')
    {
    	/*
			to get tweets from twitter API , I used TwitterOAuth 
			the library is stored in plugins folder
			See documentation at https://twitteroauth.com.
			configuration for this library in config/config.php
    	*/
    	$TwitterOAuth = new Abraham\TwitterOAuth\TwitterOAuth($this->config->TwitterOAuth->consumer_key, $this->config->TwitterOAuth->consumer_secret, $this->config->TwitterOAuth->access_token, $this->config->TwitterOAuth->access_token_secret);
		$parameters['q'] = $word;
		$parameters['result_type'] = $type;
		$parameters['count'] = '100';

		/*  twitter api limit max result is 100 tweets ,
		 	we need to get 10,000 tweets , we loop the process 100 times
		  	we send last tweet id using parameter $parameters['max_id'] to get the second 100 tweets before that tweet;
		 */
		for ($i=0; $i < 100 ; $i++) { 

			 $content = $TwitterOAuth->get("search/tweets",$parameters);
			 foreach ($content->statuses as $status) {

			 	$tweet  = new Tweets();
				$parameters['max_id'] = $tweet->tweet_id = $status->id;
				$tweet->tweet = $status->text;
				$tweet->datetime = $status->created_at;
				$tweet->user_uid = $status->user->id;
				$tweet->save() ;

				$tusers       = new Tusers();
			 	
			 	$tusers->name = $status->user->name;
			 	$tusers->screen_name = $status->user->screen_name;
			 	$tusers->location = $status->user->location;
			 	$tusers->description = $status->user->description;
			 	$tusers->url = $status->user->url;
			 	$tusers->followers_count = $status->user->followers_count;
			 	$tusers->friends_count = $status->user->friends_count;
			 	$tusers->created_at = $status->user->created_at;
			 	$tusers->lang = $status->user->lang;
			 	$tusers->following = $status->user->following;
			 	$tusers->save() ;
		    }
	   }
}

}
